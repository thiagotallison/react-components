# GOVBR-DS - React Components

Biblioteca de componentes React, baseados na especificação do [Padrão Digital de Governo](https://www.gov.br/ds).

## Objetivo

Construir uma biblioteca de componentes, usando [ReactJS](https://reactjs.org/), de acordo com os componentes especificados pelo [Padrão Digital de Governo](https://www.gov.br/ds).

## Tecnologias

Esse projeto é desenvolvido usando:

1. [React](https://reactjs.org/)
2. [TypeScript](https://www.typescriptlang.org/)
3. [Jest](https://jestjs.io/) & [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/)
4. [Prettier](https://prettier.io/), [ESLint](https://eslint.org/) & [EditorConfig](https://editorconfig.org/)
5. [Storybook](https://storybook.js.org/)
6. [ViteJS](https://vitejs.dev/) para empacotamento

Para saber mais detalhes sobre React sugerimos que consulte a [Documentação Oficial](https://pt-br.reactjs.org/docs/getting-started.html).

### Dependências

Para conseguir usar nossa biblioteca sem problemas instale as seguintes dependências:

### Ícones

Instalar a biblioteca de ícones Font Awesome Free:

```sh
npm install @fortawesome/fontawesome-free
```

Importar o css da biblioteca na raiz (`index.js`) do projeto:

```js
import "@fortawesome/fontawesome-free/css/all.min.css";
```

### Webfonts

Adicionar as fontes do DSGov ao `<head>` do arquivo `index.html` do projeto (ou instalar dependências):

```html
<head>
  ...
  <!-- Font Rawline-->
  <link
    rel="stylesheet"
    href="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/fonts/rawline/css/rawline.css"
  />
  <!-- Font Raleway-->
  <link
    rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"
  />
  ...
  <head></head>
</head>
```

### CSS

Importar o css do DSGov na raiz (`index.js`) do projeto:

```js
import "@govbr-ds/core/dist/core.min.css";
```

## Como usar nossa biblioteca em seu projeto?

### Instalação da biblioteca

Você pode instalar a biblioteca no seu projeto usando:

```node
npm install --save @govbr-ds/react-components
```

### Uso

Depois de instalada, importe os componentes desejados. Exemplo:

```javascript
import { BrButton } from "@govbr-ds";
```

## Exemplos de uso

Consulte o [Showcase](https://govbr-ds.gitlab.io/dev/react/react-components/showcase/) e o playgound interativo [Storybook](https://govbr-ds.gitlab.io/dev/react/react-components/storybook/).

## Documentação

Nossa documentação está disponível em <https://govbr-ds.gitlab.io/dev/react/react-components>.

## Como executar o projeto?

### Especificação Mínima de Ferramentas para desenvolver

- Node.js >= 16.x

### Instalação das dependências

Execute o script abaixo.

```node
npm install
```

Depois use os scripts abaixo.

```node
npm run <nome-do-script>
```

### Scripts disponíveis

- `generate`: Usa o [Plop](https://plopjs.com/) para criar um novo componente, usando templates pré-definidos por nós, com uma estrutura inicial de componente, testes unitários e Storybook.

Exemplo:

```bash
$ npm run generate

@govbr-ds/react-components@2.0.0 generate
plop --plopfile ./generators/plopfile.js

Nome do componente? BrTable

✔  ++ ~/react-components/src/components/BrTable/index.tsx
✔  ++ ~/react-components/src/components/BrTable/stories.tsx
✔  ++ ~/react-components/src/components/BrTable/BrTable.test.tsx
```

Informando apenas o nome do componente, serão criados os arquivos em
**~/react-components/src/components/NomeDoComponente/**

- `test`: Executa todos os testes.
- `test:watch`: Executa todos os testes em _watch mode_.
- `coverage`: Verifica a cobertura de testes do projeto.
- `dev`: Inicia uma aplicação React, em modo de desenvolvimento, permitindo testar os componentes criados em um Showcase.
- `build:lib`: Compila e empacota a biblioteca de componentes e os disponibiliza no diretório `dist`.
- `build:showcase`: Compila e empacota o Showcase para produção. Disponibiliza na pasta `showcase/` o site estático.
- `storybook`: Executa localmente a documentação do Storybook.
- `storybook:build`: Prepara documentação estática do Storybook para implantação.
- `storybook:run`: Inicia um servidor http local rodando a versão estática (produção) do Storybook.
- `lint`: Executa o ESLint para checagem estática do código.
- `commit`: Executa o Commitizen para facilitar a padronização dos commits.
- `start`: Inicia um servidor http local rodando a versão estática do Showcase (produção).

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md "Como contribuir?").

## Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/dev/react/react-components/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GOVBR-DS <http://gov.br/ds>
- React Components <https://gitlab.com/govbr-ds/dev/react/react-components/>
- Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ "Wiki") para aprender sobre os nossos padrões.

## Créditos

Os React Components do [GOVBR-DS](https://gov.br/ds/ "GOVBR-DS") são criados pelo [SERPRO](https://www.serpro.gov.br/ "SERPRO | Serviço Federal de Processamento de Dados") e [Dataprev](https://www.dataprev.gov.br/ "Dataprev | Empresa de Tecnologia e Informações da Previdência Social") juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
