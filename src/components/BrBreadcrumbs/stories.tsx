import { Meta, Story } from "@storybook/react";
import BrBreadcrumbs from ".";

export default {
  title: "Navegação/BrBreadcrumbs",
  component: BrBreadcrumbs,
} as Meta;

const bc = [
  {
    isHome: true,
    label: "home",
    onClick: () => {
      // navegar para home
    },
  },
  {
    label: "rota",
    onClick: () => {
      // navegar para rota
    },
  },
  {
    label: "subrota",
    onClick: () => {
      // navegar para subrota
    },
  },
];

export const Simples: Story = (args) => <BrBreadcrumbs {...args} />;
Simples.args = { ...Simples.args, crumbs: bc };
