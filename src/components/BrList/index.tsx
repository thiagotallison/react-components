import classNames from "classnames";

export type BrListProps = {
  className?: string;
  title?: string;
  horizontal?: boolean;
  children: React.ReactNode;
  tabIndex?: 0 | -1;
  expanded?: string | null;
};

const BrList = (props: BrListProps) => {
  const { className, title, horizontal, children, ...rest } = props;

  const classes = classNames("br-list", horizontal && "horizontal", className);

  return (
    <div className={classes} role="list" {...rest}>
      {title && (
        <>
          <div className="header">
            <div className="title">{title}</div>
          </div>
          <span className="br-divider"></span>
        </>
      )}
      {children}
    </div>
  );
};

export default BrList;
