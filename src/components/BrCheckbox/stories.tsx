import { Story, Meta } from "@storybook/react";

import BrCheckbox, { BrCheckboxProps } from ".";

export default {
  title: "Input/BrCheckbox",
  component: BrCheckbox,
} as Meta;

export const SemRotulo: Story<BrCheckboxProps> = (args) => <BrCheckbox {...args} />;

export const ComRotulo: Story<BrCheckboxProps> = (args) => <BrCheckbox {...args} />;
ComRotulo.args = { label: "meu check" };
