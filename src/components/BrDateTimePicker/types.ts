export type DTPickerType = "date" | "time" | "dateTime";
export type DTPickerInput = string | number | Date;
export type DTPickerOutput = Date[] | string;

export type DateOptions = {
  minDate?: string;
  maxDate?: string;
};

export type TimeOptions = {
  minuteIncrement?: number;
  enableSeconds?: boolean;
  defaultHour?: number;
  defaultMinute?: number;
};
