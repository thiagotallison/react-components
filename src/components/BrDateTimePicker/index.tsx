import { createRef, useCallback, useEffect } from "react";
import { uniqueId } from "lodash";
import flatpickr from "flatpickr";
import { Portuguese } from "flatpickr/dist/l10n/pt";

import BrInput from "@/components/BrInput";
import BrButton from "@/components/BrButton";

import Icon from "@/common/Icon";

import { getOptions } from "./helpers";
import { DTPickerType, DateOptions, TimeOptions, DTPickerInput, DTPickerOutput } from "./types";

export type BrDateTimePickerProps = {
  id?: string;
  label?: string;
  editable?: boolean;
  type?: DTPickerType;
  dateProps?: DateOptions;
  timeProps?: TimeOptions;
  defaultValue?: DTPickerInput;
  mode?: "single" | "multiple" | "range";
  outputType?: "dateArray" | "string";
  onClose: (value: DTPickerOutput) => void;
};

const BrDateTimePicker = (props: BrDateTimePickerProps) => {
  const {
    id = uniqueId("date-picker-"),
    type = "date",
    mode = "single",
    label,
    editable,
    dateProps,
    timeProps,
    defaultValue,
    outputType,
    onClose,
  } = props;

  const pickerRef = createRef<HTMLInputElement>();

  const { inputType, placeholder, icon, ...options } = getOptions(
    type,
    defaultValue,
    dateProps,
    timeProps
  );

  const handleClose = useCallback(
    (dates: Date[], dateStr: string) => {
      onClose(outputType === "dateArray" ? dates : dateStr);
    },
    [outputType, onClose]
  );

  useEffect(() => {
    if (pickerRef.current) {
      flatpickr(pickerRef.current, {
        allowInput: editable,
        locale: Portuguese,
        time_24hr: true,
        mode,
        ...options,
        onClose: handleClose,
      });
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="br-datetimepicker">
      <BrInput
        hasIcon
        ref={pickerRef}
        type={inputType}
        id={`${id}-input`}
        label={label}
        placeholder={placeholder}
      >
        <BrButton
          id={`${id}-button`}
          circle
          size="small"
          aria-label="Abrir Datepicker"
          onClick={() => pickerRef.current?.click()}
        >
          <Icon icon={icon} />
        </BrButton>
      </BrInput>
    </div>
  );
};

export default BrDateTimePicker;
