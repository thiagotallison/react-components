import Icon from "@/common/Icon";
import { MessageIcon, MessageType } from "@/types/messages";

export interface BrContextMessageProps {
  type: MessageType;
  message: string;
}

const BrContextMessage = (props: BrContextMessageProps) => {
  const { message = "", type } = props;

  return (
    <span className={`feedback ${type}`} role="alert">
      <Icon icon={MessageIcon[type]} aria-hidden="true" />
      {message}
    </span>
  );
};

export default BrContextMessage;
